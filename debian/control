Source: djangorestframework-gis
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Michael Fladischer <fladi@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 python3-all,
 python3-setuptools,
Standards-Version: 4.6.2
Homepage: https://github.com/djangonauts/django-rest-framework-gis
Vcs-Git: https://salsa.debian.org/python-team/packages/djangorestframework-gis.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/djangorestframework-gis
Rules-Requires-Root: no

Package: python3-djangorestframework-gis
Architecture: all
Depends:
 python3-django-filters,
 python3-djangorestframework,
 ${misc:Depends},
 ${python3:Depends},
Description: Geographic add-ons for Django REST Framework (Python3 version)
 djangorestframework-gis extends the Django REST Framework to also handle
 geographic data as used by GeoDjango during serialization and deserialization.
 It provides the following features:
 .
  * GeometryField: This field handles GeoDjango geometry fields, providing
      custom to_native and from_native methods for GeoJSON input/output.
  * GeoModelSerializer: This serializer updates the field_mapping dictionary to
      include field mapping of GeoDjango geometry fields to the above
      GeometryField.
  * GeoFeatureModelSerializer: GeoFeatureModelSerializer is a subclass of
      GeoModelSerializer which will output data in a format that is GeoJSON
      compatible.
  * InBBOXFilter: Filters a queryset to only those instances within a certain
      bounding box.
 .
 This package contains the Python 3 version of the library.
